# Лабораторная работа 1. Отчёт по работе
## Содержание
[[_TOC_]]
## Описание программы

Данная программа находит количество вхождений в массив максимального элемента

Модуль iarray:
* Функция `GetMaxCount` принимает массив и его длину и возвращает количество вхождений в массив максимального элемента

Модуль main:
* Функция `main` выполняет тестирование программы и вывод результатов
* Функция `test1` возвращает массив и его длину для теста №1
* Функция `test2` возвращает массив и его длину для теста №2

## Поиск ошибки

Нашел значения при которых программа работает неверно: `[1,1,1,1,5]`.
Программа возвращает: 4
Правильный ответ: 1


Проанализировал изменения с помощью команд git status и git diff. 

```bash
user-lab01@win10-lab1-38 MINGW64 ~/work
$ git status
На ветке master
Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git checkout -- <файл>…», чтобы отменить изменения
   в рабочем каталоге)

        изменено:      main.py

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)

user-lab01@win10-lab1-38 MINGW64 ~/work
$ git diff
diff --git a/main.py b/main.py
index 788e6b3..803ce92 100644
--- a/main.py
+++ b/main.py
@@ -10,12 +10,20 @@ def Test1():
   Arr.append(1)

   return Arr, 5
+  ^M
+def Test2():^M
+    Arr = [1,1,1,1,5]^M
+    return Arr, len(Arr)^M


 def main():
   Arr, N = Test1()

   print("Maximal value is found " + str(GetMaxCount(Arr, N)) + " times.")
+  ^M
+  Arr, N = Test2()^M
+  ^M
+  print("Maximal value is found " + str(GetMaxCount(Arr, N)) + " times.")^M


 if __name__ == '__main__':

[1]+  Остановлен    git diff

```

Зафиксировал изменения 
```bash
$ git commit -m "add test"
[master 5947b7e] add test
 1 file changed, 8 insertions(+)

```

## Исправление ошибки

Исправил ошибку и проверил правильность выполнения программы.

```bash
user-lab01@win10-lab1-38 MINGW64 ~/work
$ python main.py
Maximal value is found 2 times.
Maximal value is found 1 times.

```

Проанализировал изменения:
```bash
user-lab01@win10-lab1-38 MINGW64 ~/work
$ git status
На ветке master
Изменения, которые не в индексе для коммита:
  (используйте «git add <файл>…», чтобы добавить файл в индекс)
  (используйте «git checkout -- <файл>…», чтобы отменить изменения
   в рабочем каталоге)

        изменено:      iarray.py

нет изменений добавленных для коммита
(используйте «git add» и/или «git commit -a»)

user-lab01@win10-lab1-38 MINGW64 ~/work
$ git diff
diff --git a/iarray.py b/iarray.py
index bfe46b8..08acd73 100644
--- a/iarray.py
+++ b/iarray.py
@@ -6,6 +6,7 @@ def GetMaxCount(Arr, N):
   while (I < N):
     if (Arr[I] > Max):
       Max = Arr[I]
+      Count = 1^M
     else:
       if (Max == Arr[I]):
         Count += 1;

```

Зафиксировал изменения
```bash
user-lab01@win10-lab1-38 MINGW64 ~/work
$ git add iarray.py

user-lab01@win10-lab1-38 MINGW64 ~/work
$ git status
На ветке master
Изменения, которые будут включены в коммит:
  (используйте «git reset HEAD <файл>…», чтобы убрать из индекса)

        изменено:      iarray.py


user-lab01@win10-lab1-38 MINGW64 ~/work
$ git commit -m "fix mistake in test 2"
[master 14a923b] fix mistake in test 2
 1 file changed, 1 insertion(+)
```
## Анализ истории
### Проанализируйте историю изменений с помощью команды git log.
```
$ git log
commit 91da73bcc9418c6ada3d5d350932d617dda7dc6d (HEAD -> lab1, origin/lab1)
Author: Vladimir Larin <denixed@ya.ru>
Date:   Thu Feb 13 17:27:03 2020 +0300

    add readme.md

commit 14a923bf1bce67e2b9fc1b3283eb0d8a7d977410
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:27:58 2020 +0300

    fix mistake in test 2

commit 5947b7e346ed7a119b0c568b2286a287c65bc0bc
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:25:53 2020 +0300

    add test

commit 14454a31a2f3691da6726473f97b1471070055bd
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:15:24 2020 +0300

    Inital version of program was added.

commit f8ed4a4798d5b639443987ca22161edfa4e2e6c6
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:14:25 2020 +0300

    .gitinore was added.
```
### Что изменится в выдаче этой команды, если вы добавите параметр “—name-status”?
Добавится информация в какие файлы были внесены изменения. Возможные статусы:
* Added (A) - файл добавлен
* Copied (C) - файл скопирован
* Deleted (D) - файл удалён
* Modified (M) - файл изменён
* Renamed (R) - файл переименован
```
$ git log --name-status
commit 91da73bcc9418c6ada3d5d350932d617dda7dc6d (HEAD -> lab1, origin/lab1)
Author: Vladimir Larin <denixed@ya.ru>
Date:   Thu Feb 13 17:27:03 2020 +0300

    add readme.md

A       readme.md

commit 14a923bf1bce67e2b9fc1b3283eb0d8a7d977410
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:27:58 2020 +0300

    fix mistake in test 2

M       iarray.py

commit 5947b7e346ed7a119b0c568b2286a287c65bc0bc
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:25:53 2020 +0300

    add test

M       main.py

commit 14454a31a2f3691da6726473f97b1471070055bd
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:15:24 2020 +0300

    Inital version of program was added.

A       iarray.py
A       main.py

commit f8ed4a4798d5b639443987ca22161edfa4e2e6c6
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:14:25 2020 +0300

    .gitinore was added.

A       .gitignore
```
### С помощью какого параметра, можно анализировать историю не за весь период, а, например, между двумя определенными ревизиями?
Можно у команды `git log` указать номера двух ревизий, от которой до которой (не включительно) нужно отобразить историю.
```
$ git log 14454a31a2f3691da6726473f97b1471070055bd..14a923bf1bce67e2b9fc1b3283eb0d8a7d977410
commit 14a923bf1bce67e2b9fc1b3283eb0d8a7d977410
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:27:58 2020 +0300

    fix mistake in test 2

commit 5947b7e346ed7a119b0c568b2286a287c65bc0bc
Author: Vladimir Larin <denixed@ya.ru>
Date:   Fri Feb 7 14:25:53 2020 +0300

    add test
```
### Каким образом можно сравнить две версии одного и того же файла, но разных ревизий?
Можно использовать команду `git diff` с указанием номеров двух ревизий и названия файла.
```
$ git diff 14454a31a2f3691da6726473f97b1471070055bd 14a923bf1bce67e2b9fc1b3283eb0d8a7d977410 main.py
diff --git a/main.py b/main.py
index 788e6b3..803ce92 100644
--- a/main.py
+++ b/main.py
@@ -10,12 +10,20 @@ def Test1():
   Arr.append(1)

   return Arr, 5
+  ^M
+def Test2():^M
+    Arr = [1,1,1,1,5]^M
+    return Arr, len(Arr)^M


 def main():
   Arr, N = Test1()

   print("Maximal value is found " + str(GetMaxCount(Arr, N)) + " times.")
+  ^M
+  Arr, N = Test2()^M
+  ^M
+  print("Maximal value is found " + str(GetMaxCount(Arr, N)) + " times.")^M


 if __name__ == '__main__':
```
### Научитесь анализировать историю изменений с помощью GitLab.
В гитлабе можно просматривать историю коммитов.
![image](uploads/e88047cbce3d7373350f6ef1c00ba952/image.png)
Можно просматривать какие изменения были внесены в коммит.
![image](uploads/81606b2abb2f1da7e4e769068ce7c5ce/image.png)
Можно сравнить разные ветки.
![image](uploads/9fa371fb8291945556f0e81dfc6e2392/image.png)
## Работа с вики
### Добавление рисунков
Изображения добавляются следующим образом
```md
Запись в одну строку:

![альтернативный текст](uploads/4ffd7dd5cf5244a0a2b585389c5ada1d/markdown_logo.png "Подсказка при наведение")

Запись с указание источника:

![альтернативный текст][logo]

[logo]: uploads/4ffd7dd5cf5244a0a2b585389c5ada1d/markdown_logo.png "Подсказка при наведение"
```
**Пример:**
Запись в одну строку:

![альтернативный текст](uploads/4ffd7dd5cf5244a0a2b585389c5ada1d/markdown_logo.png "Подсказка при наведение")

Запись с указание источника:

![альтернативный текст][logo]

[logo]: uploads/4ffd7dd5cf5244a0a2b585389c5ada1d/markdown_logo.png "Подсказка при наведение"

### Оглавление
Автособираемое оглавление можно сделать следующим образом:
```md
[[_TOC_]]
```
**Пример:**

[[_TOC_]]

### Ссылки для перехода между страницами wiki
Ссылки для перехода между страницами wiki оформляются так же, как и обычные ссылки за исключение того, что используется относительное адресация.
```
[Лабораторная работа 1 (знакомство с wiki)](lab_01_test)
```
**Пример:**
[Лабораторная работа 1 (знакомство с wiki)](lab_01_test)

### Цитаты
Цитаты могут быть двух типов:
- однострочные
- многострочные
Пример оформления:
```md
> Какой-то комментарий в одну строку.
> Продолжение цитаты

> Новая цитата

>>>
Много строчная цитата

Делается так
>>>
```

> Какой-то комментарий в одну строку.
> Продолжение цитаты

> Новая цитата

>>>
Много строчная цитата

Делается так
>>>
### Стили оформления таблиц

```md
Центрирование:

| Работа        | с             | табличкой|
| ------------- |:-------------:| --------:|
| col 3 is      | right-aligned |    $1600 |
| col 2 is      | centered      |      $12 |
| zebra stripes | are neat      |       $1 |

Должно быть как минимум три тире, разделяющий заголовок от тела таблицы.
Внешние | не обязательны 

Простой| стиль| таблицы
--- | --- | ---
*Также* | `отображается` | **красиво**
1 | 2 | 3
```
Центрирование:

| Работа        | с             | табличкой|
| ------------- |:-------------:| --------:|
| col 3 is      | right-aligned |    $1600 |
| col 2 is      | centered      |      $12 |
| zebra stripes | are neat      |       $1 |

Должно быть как минимум три тире, разделяющий заголовок от тела таблицы.
Внешние | не обязательны 

Простой| стиль| таблицы
--- | --- | ---
*Также* | `отображается` | **красиво**
1 | 2 | 3




