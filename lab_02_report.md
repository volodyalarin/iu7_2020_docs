# Лабораторная работа 2. Отчёт по работе
## Содержание
[[_TOC_]]
## Задача 1
Иницилизировал репозиторий.
```
$ git init
Initialized empty Git repository in /mnt/c/Users/volodya/Desktop/ОПИ/lab2/.git/
```
Добавил файл .gitignore, чтобы устранить случайное попадание временный файлов работы программ на языке программирования питон (`**/__pycache__/**`) 
```
$ git add .gitignore 
$ git commit -m "add gitignore"
[master (root-commit) 868b5e7] add gitignore
 1 file changed, 1 insertion(+)
 create mode 100644 .gitignore
```
Добавил исходный код программы.
```
$ python3 main.py
Source
0 -1 3 -2 5
Result
0 3 5 
```
Зафиксировал изменения, чтобы файлы программы попали по версионный контроль.
```
$ git add *.py
$ git commit -m "add source files"
[master 51c0085] add source files
 2 files changed, 49 insertions(+)
 create mode 100644 iarray.py
 create mode 100644 main.py
```
Создал новую ветку для исправления ошибок, чтобы не нарушать работу стабильной версии.
```
$ git checkout -b fix
Switched to a new branch 'fix'
```
Добавил комментарии к функциям для удобства разработки и написал тест, на котором программа ведёт себя не верно.
```
$ git add .
$ git diff --cached
diff --git a/iarray.py b/iarray.py
index 63b016d..58f1a62 100644
--- a/iarray.py
+++ b/iarray.py
@@ -1,4 +1,8 @@
 def form_array(arr, n):
+  # Функция удаляет из массива отрицательные элементы
+  # arr - старый массив
+  # n - длинна массива arr
+
   new_arr = arr

   i = 0
@@ -13,6 +17,10 @@ def form_array(arr, n):


 def print_array(arr, n):
+  # Функция выводит содержимое массива в консоль
+  # arr - старый массив
+  # n - длинна массива arr
:...skipping...
diff --git a/iarray.py b/iarray.py
index 63b016d..58f1a62 100644
--- a/iarray.py
+++ b/iarray.py
@@ -1,4 +1,8 @@
 def form_array(arr, n):
+  # Функция удаляет из массива отрицательные элементы
+  # arr - старый массив
+  # n - длинна массива arr
+
   new_arr = arr

   i = 0
@@ -13,6 +17,10 @@ def form_array(arr, n):


 def print_array(arr, n):
+  # Функция выводит содержимое массива в консоль
+  # arr - старый массив
+  # n - длинна массива arr
+
   i = 0
   while (i < n):
     print(arr[i], end = " ")
diff --git a/main.py b/main.py
index 3a33132..dbb9ab1 100644
--- a/main.py
+++ b/main.py
@@ -1,6 +1,8 @@
 from iarray import *

 def test_1():
+  # Данные для теста 1
+
```
Проверил выполнение программы. Нашёл дефект работы алгоритма. На вход программы подаётся `[-1, -1, -3, -3, 4]`, программа выдаёт `[-1, -3, 4]`, а должна `[4]`. 
```
$ python3 main.py 
Source
0 -1 3 -2 5
Result
0 3 5
Source 2
-1 -1 -3 -3 4
Result 2
-1 -3 4
```
Зафиксировал изменения, где добавил провалившийся тест.
```
$ git add .
$ git commit -m "add failed test"
[fix 11513ce] add failed test
 2 files changed, 29 insertions(+)
```

Создал обсуждение #2 в ситеме Gitlab для коммита 11513cec92e81599709a3119ecf533612eeb3592 и сообщил о неверной работе алгоритма, обнаруженной во время тестирования программы. 



Исправил ошибку. Проблема заключалась в том, что при удалении элемента в python сдвигались элементы на одну позицию, из-за чего программа проверяла не все элементы массива. Исправил, добавив декрементирование счётчика. 
```
$ python3 main.py 
Source
0 -1 3 -2 5
Result
0 3 5
Source 2
-1 -1 -3 -3 4
Result 2
4

$ git diff 
diff --git a/iarray.py b/iarray.py
index 58f1a62..fcc73d9 100644
--- a/iarray.py
+++ b/iarray.py
@@ -10,6 +10,7 @@ def form_array(arr, n):
     if (new_arr[i] < 0):
       new_arr.pop(i)
       n -= 1
+      i -= 1

     i += 1
```
Зафиксировал внесенные правки в алгоритм работы программы, устраняющие проблему проблемму, опписанную в issue #2.

```
$ git add .
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/ОПИ/lab2$ git commit -m "fix error"
[fix 0cb85a9] fix error
 1 file changed, 1 insertion(+)
```

Написал в обсуждение #2 об исправление ошибки и суть измененений.

Ревизию 0cb85a9a669027fbd33fdaf060e79745ea29635c перенес в мастер ветку. Конфликтов не возникло.

```
$ git checkout master
Switched to branch 'master'
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/ОПИ/lab2$ git merge fix
Updating 51c0085..0cb85a9
Fast-forward
 iarray.py |  9 +++++++++
 main.py   | 21 +++++++++++++++++++++
 2 files changed, 30 insertions(+)
```
Проанализировал историю изменений в репозитории. Конфликты не возникли, так как разработка велась линейно, т.е. не было внесено никаких изменений в мастер ветку во время работы с fix веткой.
```
$ git log --graph --oneline --all
* 0cb85a9 (HEAD -> master, fix) fix error
* 11513ce add failed test
* 51c0085 add source files
* 868b5e7 add gitignore
```

## Задача 2

Проанализируйте историю изменений в этом репозитории.
a. Сколько ветвей в нем есть? Как они называются?
2 ветви в репозитории:
- develop
- master
```
$ git branch
  develop
* master
```
b. Сколько пользователей работали с этим репозиторием? Какие имена у этих пользователей?

2 пользователя работали над проектом:
- Minstrel
- Songster
```
$ git shortlog  --email --all
Minstrel <minstrel@test> (4):
      Beginning.
      Couplet #1.
      Couplet #3.
      Couplet #4.

Songster <songster@test> (2):
      Couplet #2.
      Couplets #5 and #6.
```

c. Сколько файлов находится в репозитории? Кто и в какой последовательности вносил изменения в эти файлы?

```
$ git log --all --graph
* commit a52c4ec45e14d5b48095e1bc9c569f06f4838362 (develop)
| Author: Songster <songster@test>
| Date:   Tue Mar 13 17:03:38 2018 +0300
|
|     Couplets #5 and #6.
|
* commit 2c605117c7fe0fc2452e787b44519f212ce48bcc
| Author: Songster <songster@test>
| Date:   Tue Mar 13 16:59:09 2018 +0300
|
|     Couplet #2.
|   
| * commit 1295f707870f0e1c13e9144b03cbef4bd10b52a7 (HEAD -> master)
| | Author: Minstrel <minstrel@test>
| | Date:   Tue Mar 13 17:01:15 2018 +0300
| |
| |     Couplet #4.
| |
| * commit 4fcbc684b3e8a22c172a218107bd669e0768b9f4
| | Author: Minstrel <minstrel@test>
| | Date:   Tue Mar 13 17:00:30 2018 +0300
| |
| |     Couplet #3.
| |
| * commit 122ee3aeeea5f70e6a933bf0f4afbaa436e57b25
|/  Author: Minstrel <minstrel@test>
|   Date:   Tue Mar 13 16:57:10 2018 +0300
|
|       Couplet #1.
| 
* commit f739d2e40e87639a7fbb1d2a1333944164f879be
  Author: Minstrel <minstrel@test>
  Date:   Tue Mar 13 16:50:47 2018 +0300

      Beginning.
```
Пробую объединить ветки. Возникла ошибка слияния, так как в обоих ветках произошли изменения в однихи тех же файлах с момента создания новой ветки. 
```
$ git merge develop
Auto-merging song.txt
CONFLICT (content): Merge conflict in song.txt
Automatic merge failed; fix conflicts and then commit the result.
```
Устранил конфликт, расставив куплеты в нужном порядке, и зафиксировал изменения.
```
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

        both modified:   song.txt

no changes added to commit (use "git add" and/or "git commit -a")
$ git add .
$ git commit -m "merge song"
[master 8d668f1] merge song
```

## Задача 3
### Задача 3.1
Создал ветвь , поместил в него исходный код решенных задач,
отправил изменения в удаленный репозиторий, создал [merge request](https://git.iu7.bmstu.ru/iu7-cprog/iu7-cprog-labs-2020/iu7-cprog-labs-2020-larinvladimir/merge_requests/1).


### Задача 3.2

Получил копию удаленного репозитория и перешёл в рабочую директорию, чтобы продолжить разработку над проектом.
```
$ git clone gitlab@git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git temp_work
Cloning into 'temp_work'...
remote: Enumerating objects: 20, done.
remote: Counting objects: 100% (20/20), done.
remote: Compressing objects: 100% (16/16), done.
remote: Total 20 (delta 4), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (20/20), done.
Resolving deltas: 100% (4/4), done.
$ cd temp_work
```
На основе мастер ветки создал ветки lab_02_a и lab_02_b.

```
$ git branch lab_02_a
$ git branch lab_02_b
$ git branch
  lab_02_a
  lab_02_b
* master
```
Переключился на ветвь lab_02_a, чтобы имел возможно работать в этой ветке.
```
$ git checkout lab_02_a
Switched to branch 'lab_02_a'
```

Добавил под версионный контроль:
- текстовый файл lab_02_a.txt, который содержит текст “lab_02_a”;
- файл .gitignore для игнорирования исполняемых файлов.
```
$ git add .
$ git commit -m "add .gitignore and lab_02_a.txt"   
[lab_02_a 505b828] add .gitignore and lab_02_a.txt
 2 files changed, 3 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 lab_02_a.txt
```
Отправил изменения в удаленный репозиторий, чтобы изменения сохранились на удаленном сервере.

```
$ git push --set-upstream origin lab_02_a
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (4/4), 357 bytes | 32.00 KiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
remote: 
remote: To create a merge request for lab_02_a, visit:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/new?merge_request%5Bsource_branch%5D=lab_02_a
remote: 
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
 * [new branch]      lab_02_a -> lab_02_a
Branch 'lab_02_a' set up to track remote branch 'lab_02_a' from 'origin'.
```

Создал запрос на слияние !1 , чтобы произвести объединение мастер ветки и lab_02_a на удаленном репозитории. (к тому же нет доступа записи в мастер ветки)

Переключитесь на ветвь lab_02_b, чтобы вносить изменения в данную ветку.
```
$ git checkout lab_02_b
Switched to branch 'lab_02_b'
```

Добавил под версионный контроль
- текстовый файл lab_02_b.txt, который содержит текст “lab_02_b”;
- файл .gitignore для игнорирования объектных файлов
```
$ git add .
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/ОПИ/temp_work$ git commit -m "add .gitignore and lab_02_b.txt"
[lab_02_b a77ed3c] add .gitignore and lab_02_b.txt
 2 files changed, 2 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 lab_02_b.txt
```
Отправил изменения в удаленный репозиторий и создал merge request !2, чтобы объединить ветки lab_02_b и мастер ветки.
```
$ git push --set-upstream origin lab_02_b
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (4/4), 345 bytes | 31.00 KiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
remote: 
remote: To create a merge request for lab_02_b, visit:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/new?merge_request%5Bsource_branch%5D=lab_02_b
remote:
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
 * [new branch]      lab_02_b -> lab_02_b
Branch 'lab_02_b' set up to track remote branch 'lab_02_b' from 'origin'.
```
Преподаватель принял запрос на слияние !2. Объединил ветки в локальном репозитории, это нужно чтобы синхронизировать локальный и внешние репозитории.
```
$ git pull -a
remote: Enumerating objects: 1, done.
remote: Counting objects: 100% (1/1), done.
remote: Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (1/1), done.
From git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir
   e2dbb60..bb17276  master     -> origin/master
Your configuration specifies to merge with the ref 'refs/heads/lab_02_b'
from the remote, but no such ref was fetched.

$ git log --graph --all
*   commit bb17276f434d1010af208762dc2c98fc1d997c69 (origin/master, origin/HEAD)
|\  Merge: e2dbb60 a77ed3c
| | Author: Nikulshina Tatiana <tnikulshina@gmail.com>
| | Date:   Tue Feb 25 21:38:36 2020 +0300
| |
| |     Merge branch 'lab_02_b' into 'master'
| |
| |     Запрос на объединение B. (примите его, пожалуйста)
| |
| |     See merge request iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir!2
| |
| * commit a77ed3c2bd9b85893bd127fe61702e14e461483c (HEAD -> lab_02_b, origin/lab_02_b)
|/  Author: Volodya Larin <denixed@ya.ru>
|   Date:   Sat Feb 22 00:35:22 2020 +0300
|
|       add .gitignore and lab_02_b.txt
|
| * commit 505b8282f50844bd3615a986c1b8f0767126781d (origin/lab_02_a, lab_02_a)
|/  Author: Volodya Larin <denixed@ya.ru>
|   Date:   Sat Feb 22 00:28:39 2020 +0300
|
|       add .gitignore and lab_02_a.txt
|
* commit e2dbb60cbaedbc6187b02bf8b27c8fbb5c99ce8b (master)
  Author: Olenev Anton <aolenev@bmstu.ru>
  Date:   Fri Feb 7 21:24:39 2020 +0300

      initial commit

* commit 1b3767a628e47d3463a6c99a70063187e498d15a (origin/lab1)
| Author: Larin Vladimir <810-lvn19u232@users.noreply.git.iu7.bmstu.ru>
| Date:   Sat Feb 15 13:07:09 2020 +0300
|
|     Delete readme.md. Replace it in gitlab wiki
|
* commit 91da73bcc9418c6ada3d5d350932d617dda7dc6d
| Author: Vladimir Larin <denixed@ya.ru>
| Date:   Thu Feb 13 17:27:03 2020 +0300
|
|     add readme.md
|
* commit 14a923bf1bce67e2b9fc1b3283eb0d8a7d977410
| Author: Vladimir Larin <denixed@ya.ru>
| Date:   Fri Feb 7 14:27:58 2020 +0300
|
|     fix mistake in test 2
|
* commit 5947b7e346ed7a119b0c568b2286a287c65bc0bc
| Author: Vladimir Larin <denixed@ya.ru>
| Date:   Fri Feb 7 14:25:53 2020 +0300
| 
|     add test
| 
* commit 14454a31a2f3691da6726473f97b1471070055bd
| Author: Vladimir Larin <denixed@ya.ru>
| Date:   Fri Feb 7 14:15:24 2020 +0300
| 
|     Inital version of program was added.
| 
* commit f8ed4a4798d5b639443987ca22161edfa4e2e6c6
  Author: Vladimir Larin <denixed@ya.ru>
  Date:   Fri Feb 7 14:14:25 2020 +0300
  
      .gitinore was added.

```


Сливаю ветки мастер и lab_02_a. Возник конфликт. Сливаю именно master -> lab_02_a, так как у разработчика нет доступа к удалённой мастер ветки, а конфликт устранить нужно. 

```
$ git merge master
Auto-merging .gitignore
CONFLICT (add/add): Merge conflict in .gitignore
Automatic merge failed; fix conflicts and then commit the result.
```
Устранил конфликт, объединив две версии файла `.gitignore`. 

```
$ git diff
diff --cc .gitignore
index 1530978,9cc18da..0000000
mode 100644,100644..100755
--- a/.gitignore
+++ b/.gitignore
@@@ -1,1 -1,2 +1,3 @@@
- *.o
++*.o
+ *.exe
+ *.out
```
Зафиксировал изменения, чтобы подготовить к загрузке на удаленный сервер.
```
$ git add .
$ git commit -m "fix conflict"
[lab_02_a fe2e761] fix conflict
```

Отправил измения на удалённые сервер, чтобы устранить конфликт в запросе на слияние !1

```
$ git push origin lab_02_a
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 383 bytes | 76.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: 
remote: View merge request for lab_02_a:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/1
remote:
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
   505b828..fe2e761  lab_02_a -> lab_02_a

```

### Задача 3.3

Получил копию удаленного репозитория и перешел в рабочую директорию.

```
$ git clone gitlab@git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git temp-work-2
Cloning into 'iu7-software-engineering-labs-2020-larinvladimir'...
remote: Enumerating objects: 32, done.
remote: Counting objects: 100% (32/32), done.
remote: Compressing objects: 100% (23/23), done.
remote: Total 32 (delta 5), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (32/32), 4.51 KiB | 577.00 KiB/s, done.
Resolving deltas: 100% (5/5), done.
$ cd temp-work-2
```
На основе мастер ветки создал ветку lab_02_c и переключился на неё.
```
$ git checkout -b lab_02_c
Switched to a new branch 'lab_02_c'
```

Добавил под версионный контроль текстовый файл lab_02_c. Текстовый файл содержит абзац #1 текста.
```
$ git add lab_02_c.txt
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/ОПИ/temp-work-2$ git diff --cached
diff --git a/lab_02_c.txt b/lab_02_c.txt
new file mode 100644
index 0000000..2b4bcb5
--- /dev/null
+++ b/lab_02_c.txt
@@ -0,0 +1,9 @@
+// Абзац #1
+Вот оно какое, наше лето,
+Лето яркой зеленью одето,
+Лето жарким солнышком согрето,
+Дышит лето ветерком.
+Ля-ля-ля ля-ля-ля,
+Ля-ля-ля-ля-ля ля-ля-ля-ля.
+Ля-ля-ля ля-ля-ля,
+Ля-ля-ля-ля-ля ля-ля!
$ git commit -m "add first couplet"  
[lab_02_c 6d94ad3] add first couplet
 1 file changed, 9 insertions(+)
 create mode 100644 lab_02_c.txt
```

Отправил изменения в удалённый репозиторий, чтобы сохранить изменения на удаленный сервер. 
```
$ git push --set-upstream origin lab_02_c
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 422 bytes | 211.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote: 
remote: To create a merge request for lab_02_c, visit:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/new?merge_request%5Bsource_branch%5D=lab_02_c      
remote:
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
 * [new branch]      lab_02_c -> lab_02_c
Branch 'lab_02_c' set up to track remote branch 'lab_02_c' from 'origin'.
```

Добавил в файл абзац #3 текста, зафиксировал изменения в локальном репозитории без отправки (по условию задачи) в удаленный репозиторий.

```
$ git diff
diff --git a/lab_02_c.txt b/lab_02_c.txt
--- a/lab_02_c.txt
+++ b/lab_02_c.txt
@@ -7,3 +7,12 @@
 Ля-ля-ля-ля-ля ля-ля-ля-ля.
 Ля-ля-ля ля-ля-ля,
 Ля-ля-ля-ля-ля ля-ля!
+// Абзац #3
+Мы покрыты бронзовым загаром,
+Ягоды в лесу горят пожаром.
+Лето это жаркое недаром,
+Лето — это хорошо!
+Ля-ля-ля ля-ля-ля,
+Ля-ля-ля-ля-ля ля-ля-ля-ля.
+Ля-ля-ля ля-ля-ля,
+Ля-ля-ля-ля-ля ля-ля!
$ git commit -a -m "add third couplet"
[lab_02_c c7b3ca5] add third couplet
 1 file changed, 9 insertions(+)
 ```

Склонировал удаленный репозиторий в другую директорию, чтобы сыметировать работу из разных мест, например из стен университета и комнаты общежития)) . 

 ```
 $ git clone gitlab@git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git temp-work-3
Cloning into 'temp-work-3'...
remote: Enumerating objects: 35, done.
remote: Counting objects: 100% (35/35), done.
remote: Compressing objects: 100% (26/26), done.
remote: Total 35 (delta 6), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (35/35), 4.87 KiB | 711.00 KiB/s, done.
Resolving deltas: 100% (6/6), done.
$ cd temp-work-3
```

Переключился на ветку lab_02_c, чтобы имел возможность вносить изменения в эту ветку. 

```
$ git checkout lab_02_c
Branch 'lab_02_c' set up to track remote branch 'lab_02_c' from 'origin'.
Switched to a new branch 'lab_02_c'
```

Добавил в текстовый файл абзац #2, выполнил фиксацию изменений, после чего отправил изменения в удаленный репозиторий.

```
$ git diff
diff --git a/lab_02_c.txt b/lab_02_c.txt
index 2b4bcb5..6115517 100644
--- a/lab_02_c.txt
+++ b/lab_02_c.txt
@@ -7,3 +7,15 @@
 Ля-ля-ля-ля-ля ля-ля-ля-ля.
 Ля-ля-ля ля-ля-ля,
 Ля-ля-ля-ля-ля ля-ля!
+// Абзац #2
+На зеленой солнечной опушке
+Прыгают зеленые лягушки
+И танцуют бабочки-подружки,
+Расцветает все кругом.
+Мы в дороге с песенкой о лете,
+Самой лучшей песенкой на свете,
+Мы в лесу ежа, быть может, встретим,
+Хорошо, что дождь прошел.
+Ля-ля-ля ля-ля-ля,
+Ля-ля-ля ля-ля-ля-ля.
+
$ git commit -a -m "add second couplet"
[lab_02_c 1b1b66c] add second couplet
 1 file changed, 12 insertions(+)
$ git push
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 613 bytes | 306.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for lab_02_c, visit:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/new?merge_request%5Bsource_branch%5D=lab_02_c
remote:
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
   6d94ad3..1b1b66c  lab_02_c -> lab_02_c
```
Перешёл в первую копию удаленного репозитория, чтобы продолжить иметацию работы из разных мест.  Попытался отправить изменения (изменения уже были зафиксированы). Произошла ошибка, потому что уже были внесены изменения в удалённом репозитории.
```
$ git push
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
 ! [rejected]        lab_02_c -> lab_02_c (fetch first)
error: failed to push some refs to 'gitlab@git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```
Устраняю конфликт, расставив обзацы в правильном порядке. Фиксириую изменения и отправляю их в удалённый репозиторий, чтобы сохранить изменения на удаленном сервере.

```
$ git pull
Auto-merging lab_02_c.txt
CONFLICT (content): Merge conflict in lab_02_c.txt
Automatic merge failed; fix conflicts and then commit the result.
$ git diff
diff --cc lab_02_c.txt
index 0921ea4,6115517..0000000
mode 100644,100644..100755
--- a/lab_02_c.txt
+++ b/lab_02_c.txt
@@@ -7,12 -7,15 +7,24 @@@
  Ля-ля-ля-ля-ля ля-ля-ля-ля.
  Ля-ля-ля ля-ля-ля,
  Ля-ля-ля-ля-ля ля-ля!
+ // Абзац #2
+ На зеленой солнечной опушке
+ Прыгают зеленые лягушки
+ И танцуют бабочки-подружки,
+ Расцветает все кругом.
+ Мы в дороге с песенкой о лете,
+ Самой лучшей песенкой на свете,
+ Мы в лесу ежа, быть может, встретим,
+ Хорошо, что дождь прошел.
+ Ля-ля-ля ля-ля-ля,
+ Ля-ля-ля ля-ля-ля-ля.
+ 
 +// Абзац #3
 +Мы покрыты бронзовым загаром,
 +Ягоды в лесу горят пожаром.
 +Лето это жаркое недаром,
 +Лето — это хорошо!
 +Ля-ля-ля ля-ля-ля,
 +Ля-ля-ля-ля-ля ля-ля-ля-ля.
 +Ля-ля-ля ля-ля-ля,
- Ля-ля-ля-ля-ля ля-ля!
++Ля-ля-ля-ля-ля ля-ля!
$ git commit -a -m "fix merge confilect"
[lab_02_c 057960d] fix merge confilect
$ git commit -a -m "fix merge conflict" --amend
[lab_02_c d4456c1] fix merge conflict
 Date: Thu Feb 27 20:46:30 2020 +0300

$ git push
Counting objects: 6, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 704 bytes | 352.00 KiB/s, done.
Total 6 (delta 4), reused 0 (delta 0)
remote: 
remote: To create a merge request for lab_02_c, visit:
remote:   https://git.iu7.bmstu.ru/iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir/merge_requests/new?merge_request%5Bsource_branch%5D=lab_02_c      
remote:
To git.iu7.bmstu.ru:iu7-software-engineering/iu7-software-engineering-labs-2020/iu7-software-engineering-labs-2020-larinvladimir.git
   1b1b66c..d4456c1  lab_02_c -> lab_02_c
```

Создал запрос на слияние !3, чтобы перенести изменения в мастер ветку. 