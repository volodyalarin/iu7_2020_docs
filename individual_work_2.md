# Самостоятельная работа №2.

[[_TOC_]]

## Вверх. Задача 1.
![ROOT](individual_work_2/task1/01_A0.svg)

### А0. 
![ROOT](individual_work_2/task1/02_A0.svg)

#### А1. 
![A1](individual_work_2/task1/03_A1.svg)

#### А2. 
![A2](individual_work_2/task1/04_A2.svg)

## Вверх. Задача 2.
![ROOT](individual_work_2/task2/01_A0.svg)

### А0. 
![ROOT](individual_work_2/task2/02_A0.svg)

#### А3. 
![A3](individual_work_2/task2/03_A3.svg)


