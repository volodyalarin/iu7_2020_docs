# Самостоятельная работа №1.

## Содержание
[[_TOC_]]

## 1. Результаты выполнения задания необходимо фиксировать в отчете. Ответы размещаются на отдельной странице отчета.

Создал страницу вики в гитлабе.

## 2. Что такое репозиторий? Что отличает репозиторий от сетевой папки, к которой имеют доступ все участники проекта?

Репозиторий - это хранилище, где данные проекта представлены снимками, называющиеся коммитами, которые могут размещаться на разных машинах.

Сетевая папка в отличие от git репозитория представляет только структуру файлов и содержимое этих файлов.

## 3. Какие смысловые «части» выделяет Git в директории проекта?

- сами файлы
- директория .git, в данной папке хранятся  служебные данные
  - проиндексированные изменения (коммиты)
  - журналы изменений
  - последовательность изменений
  - и т.д.

## 4. На основании чего Git понимает, что в этой директории проекта файлы находится под версионным контролем?

Git хранит информацию об этом в своей директории и узнает, что эти файлы под версионным контролем, если они были добавлены под контроль с помощью команды `git add`.

## 5. Покажите всю историю изменений в вашем проекте.

Это можно сделать с помощью команды `git log --graph --oneline --all`

```
$ git log --graph --oneline 
* ed4fad1 (HEAD -> lab_02, origin/lab_02) use filter as function    
* 26e1446 fix naming of pointers
* ebb145e fix grammar error
* 961f09e (gitlab/lab_02) wip: (task7) add prototype of solution    
* 7f01eef (task6) add solution
* 1f2b3f9 (task5) fix out of test with only zero numbers
* 8380141 (task5) add restriction for only zero numbers (experiment)
* e032e35 (task5) add solution
* bcdfcda (task5) add tests
* 44c142c (task4) add solution
* 96d4cca (task4) add tests
* 36ba26e (task3) add solution
* ac9eaec (task3) add tests
* 647956c (task2) add solution
* e2e6c71 (task2) add tests
* 90714a4 fix gitlab-ci
* 74c6132 (task1) add solution
* 87e9de4 (common) add gitla-ci.yml
* ca1b9fd (task1) add tests
* 46544e5 (origin/master, origin/HEAD, gitlab/master, master) initial commit
```

## 6. Найдите файл, изменений е который вносились чаще всего. Если таких файлов несколько, для остальных заданий выберите любой их этих файлов (укажите какой файл выбрали).

Это можно сделать, просмотрев в каких коммитах, были изменены какие файлы.

```
$ git log --oneline --name-status       
ed4fad1 (HEAD -> lab_02, origin/lab_02) use filter as function
M       lab_02_3_2/main.c
M       lab_02_3_2/untils.c
M       lab_02_3_2/untils.h
26e1446 fix naming of pointers
M       lab_02_1_5/untils.c
M       lab_02_1_6/untils.c
ebb145e fix grammar error
M       lab_02_1_5/untils.c
M       lab_02_1_6/untils.c
M       lab_02_1_6/untils.h
M       lab_02_2_4/main.c
M       lab_02_2_4/untils.c
M       lab_02_2_4/untils.h
M       lab_02_2_7/main.c
M       lab_02_2_7/untils.c
M       lab_02_2_7/untils.h
M       lab_02_3_1/array.c
M       lab_02_3_1/array.h
M       lab_02_3_1/main.c
M       lab_02_3_2/main.c
M       lab_02_3_2/untils.c
M       lab_02_3_2/untils.h
M       lab_02_3_3/main.c
M       lab_02_3_3/untils.c
M       lab_02_3_3/untils.h
961f09e (gitlab/lab_02) wip: (task7) add prototype of solution
A       lab_02_2_7/Makefile
A       lab_02_2_7/main.c
A       lab_02_2_7/tests/in_1.txt
A       lab_02_2_7/tests/in_2.txt
A       lab_02_2_7/tests/out_1.txt
A       lab_02_2_7/tests/out_2.txt
A       lab_02_2_7/untils.c
A       lab_02_2_7/untils.h
7f01eef (task6) add solution
A       lab_02_1_6/Makefile
A       lab_02_1_6/main.c
A       lab_02_1_6/untils.c
A       lab_02_1_6/untils.h
1f2b3f9 (task5) fix out of test with only zero numbers
M       lab_02_1_5/tests/out_8.txt
8380141 (task5) add restriction for only zero numbers (experiment)
M       lab_02_1_5/untils.c
e032e35 (task5) add solution
A       lab_02_1_5/Makefile
A       lab_02_1_5/main.c
A       lab_02_1_5/untils.c
A       lab_02_1_5/untils.h
bcdfcda (task5) add tests
A       lab_02_1_5/tests/in_1.txt
A       lab_02_1_5/tests/in_2.txt
A       lab_02_1_5/tests/in_3.txt
A       lab_02_1_5/tests/in_4.txt
A       lab_02_1_5/tests/in_5.txt
A       lab_02_1_5/tests/in_6.txt
A       lab_02_1_5/tests/in_7.txt
A       lab_02_1_5/tests/in_8.txt
A       lab_02_1_5/tests/out_1.txt
A       lab_02_1_5/tests/out_2.txt
A       lab_02_1_5/tests/out_3.txt
A       lab_02_1_5/tests/out_4.txt
A       lab_02_1_5/tests/out_5.txt
A       lab_02_1_5/tests/out_6.txt
A       lab_02_1_5/tests/out_7.txt
A       lab_02_1_5/tests/out_8.txt
44c142c (task4) add solution
A       lab_02_2_4/Makefile
A       lab_02_2_4/main.c
A       lab_02_2_4/untils.c
A       lab_02_2_4/untils.h
96d4cca (task4) add tests
A       lab_02_2_4/tests/in_1.txt
A       lab_02_2_4/tests/in_2.txt
A       lab_02_2_4/tests/in_3.txt
A       lab_02_2_4/tests/in_4.txt
A       lab_02_2_4/tests/in_5.txt
A       lab_02_2_4/tests/in_6.txt
A       lab_02_2_4/tests/in_7.txt
A       lab_02_2_4/tests/out_1.txt
A       lab_02_2_4/tests/out_2.txt
A       lab_02_2_4/tests/out_3.txt
A       lab_02_2_4/tests/out_4.txt
A       lab_02_2_4/tests/out_5.txt
A       lab_02_2_4/tests/out_6.txt
A       lab_02_2_4/tests/out_7.txt
36ba26e (task3) add solution
A       lab_02_3_3/Makefile
A       lab_02_3_3/main.c
A       lab_02_3_3/untils.c
A       lab_02_3_3/untils.h
ac9eaec (task3) add tests
A       lab_02_3_3/tests/in_1.txt
A       lab_02_3_3/tests/in_2.txt
A       lab_02_3_3/tests/in_3.txt
A       lab_02_3_3/tests/in_4.txt
A       lab_02_3_3/tests/in_5.txt
A       lab_02_3_3/tests/in_6.txt
A       lab_02_3_3/tests/in_7.txt
A       lab_02_3_3/tests/out_1.txt
A       lab_02_3_3/tests/out_2.txt
A       lab_02_3_3/tests/out_3.txt
A       lab_02_3_3/tests/out_4.txt
A       lab_02_3_3/tests/out_5.txt
A       lab_02_3_3/tests/out_6.txt
A       lab_02_3_3/tests/out_7.txt
647956c (task2) add solution
A       lab_02_3_2/Makefile
A       lab_02_3_2/main.c
A       lab_02_3_2/untils.c
A       lab_02_3_2/untils.h
e2e6c71 (task2) add tests
A       lab_02_3_2/tests/in_1.txt
A       lab_02_3_2/tests/in_2.txt
A       lab_02_3_2/tests/in_3.txt
A       lab_02_3_2/tests/in_4.txt
A       lab_02_3_2/tests/in_5.txt
A       lab_02_3_2/tests/in_6.txt
A       lab_02_3_2/tests/in_7.txt
A       lab_02_3_2/tests/in_8.txt
A       lab_02_3_2/tests/out_1.txt
A       lab_02_3_2/tests/out_2.txt
A       lab_02_3_2/tests/out_3.txt
A       lab_02_3_2/tests/out_4.txt
A       lab_02_3_2/tests/out_5.txt
A       lab_02_3_2/tests/out_6.txt
A       lab_02_3_2/tests/out_7.txt
A       lab_02_3_2/tests/out_8.txt
90714a4 fix gitlab-ci
R100    gitlab-ci.yml   .gitlab-ci.yml
74c6132 (task1) add solution
A       lab_02_3_1/Makefile
A       lab_02_3_1/array.c
A       lab_02_3_1/array.h
A       lab_02_3_1/main.c
87e9de4 (common) add gitla-ci.yml
A       gitlab-ci.yml
ca1b9fd (task1) add tests
A       lab_02_3_1/tests/in_1.txt
A       lab_02_3_1/tests/in_2.txt
A       lab_02_3_1/tests/in_3.txt
A       lab_02_3_1/tests/in_4.txt
A       lab_02_3_1/tests/in_5.txt
A       lab_02_3_1/tests/in_6.txt
A       lab_02_3_1/tests/in_7.txt
A       lab_02_3_1/tests/in_8.txt
A       lab_02_3_1/tests/out_1.txt
A       lab_02_3_1/tests/out_2.txt
A       lab_02_3_1/tests/out_3.txt
A       lab_02_3_1/tests/out_4.txt
A       lab_02_3_1/tests/out_5.txt
A       lab_02_3_1/tests/out_6.txt
A       lab_02_3_1/tests/out_7.txt
A       lab_02_3_1/tests/out_8.txt
46544e5 (origin/master, origin/HEAD, gitlab/master, master) initial commit
A       readme.md
```

Мы видим из этого вывода, что чаще всего изменения вносились в файл `lab_02_1_5/untils.c`

## 7. В какой ревизии этот файл появился?

```
$ git log --oneline lab_02_1_5/untils.c
26e1446 fix naming of pointers
ebb145e fix grammar error
8380141 (task5) add restriction for only zero numbers (experiment)
e032e35 (task5) add solution
```

Данный файл появился в ревизии e032e35668f3b29b5d2573293aba53245836db83. 

## 8. В какой ревизии в него были внесены последние изменения?

В данный файл были внесены изменения в ревизии 26e1446e1cbacecffa183447df8a5c8dad06b2da.

## 9. Сравните первую и последнюю ревизии этого файла.

```
$ git diff e032e35668f3b29b5d2573293aba53245836db83 26e1446e1cbacecffa183447df8a5c8dad06b2da lab_02_1_5/untils.c
diff --git a/lab_02_1_5/untils.c b/lab_02_1_5/untils.c
index 416ad73..6bc58db 100644
--- a/lab_02_1_5/untils.c
+++ b/lab_02_1_5/untils.c
@@ -8,21 +8,21 @@ int input_array(long long *array, long long **end)

     int err = INPUT_SUCCESS;

-    int lenght = 0;
+    int length = 0;

-    printf("Input lenght of array: ");
+    printf("Input length of array: ");

-    if (scanf("%d", &lenght) != 1 || lenght <= 0 || lenght > MAX_ARRAY_SIZE)
+    if (scanf("%d", &length) != 1 || length <= 0 || length > MAX_ARRAY_SIZE)
         err = INPUT_ERROR;

     if (!err)
         printf("Input array elements: ");

-    *end = array + lenght;
+    *end = array + length;

-    for (long long *i = array; i != *end && !err; i++)
+    for (long long *pi = array; pi != *end && !err; pi++)
     {
-        if (scanf("%lld", i) != 1)
+        if (scanf("%lld", pi) != 1)
             err = INPUT_ERROR;
     }

@@ -36,24 +36,24 @@ int process_array(long long *array, long long *end, long long *result)

     *result = 0;

-    long long *i = array;
-    long long *j = end - 1;
+    long long *pi = array;
+    long long *pj = end - 1;

-    while (i < end && j > array)
+    while (pi < end && pj > array)
     {
-        while (*i >= 0 && i < end) 
-            i++;
-        while (*j <= 0 && j > array) 
-            j--;
+        while (*pi >= 0 && pi < end) 
+            pi++;
+        while (*pj <= 0 && pj > array) 
+            pj--;

-        if (i < end && j > array)
-            *result += (*i) * (*j);
+        if (pi < end && pj > array)
+            *result += (*pi) * (*pj);

-        i++;
-        j--;
+        pi++;
+        pj--;
     }

-    return 0;
+    return *result == 0;
 }

 void print_result(long long result)
```

## 10. Что было изменено в предпоследнем фиксации в вашем репозитории?

```

$ git log --oneline
ed4fad1 (HEAD -> lab_02, origin/lab_02) use filter as function
26e1446 fix naming of pointers
ebb145e fix grammar error
961f09e (gitlab/lab_02) wip: (task7) add prototype of solution
7f01eef (task6) add solution
1f2b3f9 (task5) fix out of test with only zero numbers
8380141 (task5) add restriction for only zero numbers (experiment)
e032e35 (task5) add solution
bcdfcda (task5) add tests
44c142c (task4) add solution
96d4cca (task4) add tests
36ba26e (task3) add solution
ac9eaec (task3) add tests
647956c (task2) add solution
e2e6c71 (task2) add tests
90714a4 fix gitlab-ci
74c6132 (task1) add solution
87e9de4 (common) add gitla-ci.yml
ca1b9fd (task1) add tests
46544e5 (origin/master, origin/HEAD, gitlab/master, master) initial commit

$ git show 26e1446
commit 26e1446e1cbacecffa183447df8a5c8dad06b2da
Author: Volodya Larin <denixed@ya.ru>
Date:   Thu Mar 12 11:13:22 2020 +0300

    fix naming of pointers

diff --git a/lab_02_1_5/untils.c b/lab_02_1_5/untils.c
index 0d1db68..6bc58db 100644
--- a/lab_02_1_5/untils.c
+++ b/lab_02_1_5/untils.c
@@ -20,9 +20,9 @@ int input_array(long long *array, long long **end)

     *end = array + length;

-    for (long long *i = array; i != *end && !err; i++)
+    for (long long *pi = array; pi != *end && !err; pi++)
     {
-        if (scanf("%lld", i) != 1)
+        if (scanf("%lld", pi) != 1)
             err = INPUT_ERROR;
     }

@@ -36,21 +36,21 @@ int process_array(long long *array, long long *end, long long *result)

     *result = 0;

-    long long *i = array;
-    long long *j = end - 1;
+    long long *pi = array;
+    long long *pj = end - 1;

-    while (i < end && j > array)
+    while (pi < end && pj > array)
     {
-        while (*i >= 0 && i < end) 
-            i++;
-        while (*j <= 0 && j > array) 
-            j--;
+        while (*pi >= 0 && pi < end) 
+            pi++;
+        while (*pj <= 0 && pj > array) 
+            pj--;

-        if (i < end && j > array)
-            *result += (*i) * (*j);
+        if (pi < end && pj > array)
+            *result += (*pi) * (*pj);

-        i++;
-        j--;
+        pi++;
+        pj--;
     }

     return *result == 0;
diff --git a/lab_02_1_6/untils.c b/lab_02_1_6/untils.c
index 1041571..abfeedd 100644
--- a/lab_02_1_6/untils.c
+++ b/lab_02_1_6/untils.c
@@ -97,21 +97,21 @@ int process_3(long long *array, long long *end, long long *result)

     *result = 0;

-    long long *i = array;
-    long long *j = end - 1;
+    long long *pi = array;
+    long long *pj = end - 1;

-    while (i < end && j >= array)
+    while (pi < end && pj >= array)
     {
-        while (*i >= 0 && i < end) 
-            i++;
-        while (*j <= 0 && j > array) 
-            j--;
+        while (*pi >= 0 && pi < end) 
+            pi++;
+        while (*pj <= 0 && pj > array) 
+            pj--;

-        if (i < end && j > array)
-            *result += (*i) * (*j);
+        if (pi < end && pj > array)
+            *result += (*pi) * (*pj);

-        i++;
-        j--;
+        pi++;
+        pj--;
     }

     return *result == 0;

 ```
## 11. Что нужно сделать, чтобы изменить имя пользователя и/или адрес электронной почты, которое связывается с фиксацией?

Для изменения имени пользователя и/или адреса его электронной почты необходимо вызвать следующие команды:

* `git config user.name name_of_user` - для изменения имени пользователя
* `git config user.email your_email` - для изменения почты
* `git config -l` - для просмотра текущих настроек 

```
$ git config -l
user.email=denixed@ya.ru
user.mail=denixed@ya.ru
user.name=Volodya Larin
core.autocrlf=true
core.autolf=true
core.autorllf=true
core.whitespace=cr-at-eol
core.repositoryformatversion=0
core.filemode=false
core.bare=false
core.logallrefupdates=true
core.ignorecase=true
remote.origin.url=gitlab@git.iu7.bmstu.ru:iu7-cprog/iu7-cprog-labs-2020/iu7-cprog-labs-2020-larinvladimir.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
branch.lab_01.remote=origin
branch.lab_01.merge=refs/heads/lab_01
remote.gitlab.url=git@gitlab.com:denixed/cprog-labs-2020.git
remote.gitlab.fetch=+refs/heads/*:refs/remotes/gitlab/*
branch.lab_02.remote=origin
branch.lab_02.merge=refs/heads/lab_02
```

## 12. Создайте файл readme.txt, добавьте в него краткое описание цели лабораторной работы, добавьте файл под версионный контроль.

```
$ git add readme.txt 
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/iu7-cprog-labs-2020-larinvladimir$ git commit -m "(opi) add readme.txt"  
[lab_02 2ac691a] (opi) add readme.txt
 1 file changed, 1 insertion(+)
 create mode 100644 readme.txt
```

## 13. Добавьте комментарий к каждой функции файла, который описывает ее назначение В файл readme ixt добавьте на отдельной строке иши фамилию, имя и номер группы.

Выполнил

## 14. Как просмотреть изменения в этих файлах, езли файлы не в индексе?
```
$ git diff
diff --git a/lab_02_1_5/untils.c b/lab_02_1_5/untils.c
index 6bc58db..8c47c0d 100644
--- a/lab_02_1_5/untils.c
+++ b/lab_02_1_5/untils.c
@@ -3,6 +3,13 @@

 int input_array(long long *array, long long **end)
 {
+    /*
+        Функция ввода массива
+        long long *array - указатель на начало массива
+        long long **end - указатель на адрес, где хранится конец массива 
+
+        int code - возвращает код ошибки 
+    */
     if (!array || !end)
         return POINTER_ERROR;

@@ -31,6 +38,14 @@ int input_array(long long *array, long long **end)

 int process_array(long long *array, long long *end, long long *result)
 {
+    /*
+        Обработка массива
+        long long *array - указатель на начало массива
+        long long *end - указатель на конец массива
+        long long *result - указатель на поле для сохранения результата
+
+        int code - возвращает код ошибки 
+    */
     if (!array || !end || !result)
         return POINTER_ERROR;

@@ -58,5 +73,9 @@ int process_array(long long *array, long long *end, long long *result)

 void print_result(long long result)
 {
+    /*
+        Вывод результата
+        long long result - выводимый результат
+    */
     printf("Result is %lld\n", result);
 }
\ No newline at end of file
diff --git a/readme.txt b/readme.txt
index dcdbeb3..964491e 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1 +1,3 @@
+Ларин Владимир ИУ7-24Б
+
 Цель работы: научиться работать со статическими массивами (выделяемыми на стеке).
```

## 15. Поместите все файлы в индекс.

Вcе файлы в индекс можно поместить с помощью команд:

* `git add .` - добавление текущей деректории
* `git add *` - добавление всех файлов в текущей жеректории
Обе комманды равносильны.


```
$ git status
On branch lab_02
Your branch is ahead of 'origin/lab_02' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   lab_02_1_5/untils.c
        modified:   readme.txt

no changes added to commit (use "git add" and/or "git commit -a")
$ git add .
```

## 16. Как посмотреть изменения в этом случае?

Можно просмотреть с помощью команды `git diff --cached`. (добавился ключ `--cached`)

```
$ git diff --cached
diff --git a/lab_02_1_5/untils.c b/lab_02_1_5/untils.c
index 6bc58db..8c47c0d 100644
--- a/lab_02_1_5/untils.c
+++ b/lab_02_1_5/untils.c
@@ -3,6 +3,13 @@

 int input_array(long long *array, long long **end)
 {
+    /*
+        Функция ввода массива
+        long long *array - указатель на начало массива
+        long long **end - указатель на адрес, где хранится конец массива 
+
+        int code - возвращает код ошибки 
+    */
     if (!array || !end)
         return POINTER_ERROR;
     
@@ -31,6 +38,14 @@ int input_array(long long *array, long long **end)

 int process_array(long long *array, long long *end, long long *result)
 {
+    /*
+        Обработка массива
+        long long *array - указатель на начало массива
+        long long *end - указатель на конец массива
+        long long *result - указатель на поле для сохранения результата
+
+        int code - возвращает код ошибки 
+    */
     if (!array || !end || !result)
         return POINTER_ERROR;

@@ -58,5 +73,9 @@ int process_array(long long *array, long long *end, long long *result)

 void print_result(long long result)
 {
+    /*
+        Вывод результата
+        long long result - выводимый результат
+    */
     printf("Result is %lld\n", result);
 }
\ No newline at end of file
diff --git a/readme.txt b/readme.txt
index dcdbeb3..964491e 100644
--- a/readme.txt
+++ b/readme.txt
@@ -1 +1,3 @@
+Ларин Владимир ИУ7-24Б
+
 Цель работы: научиться работать со статическими массивами (выделяемыми на стеке).
```

## 17. Удалите все файлы из индекса.

Удалить файлы из индекса можно с помощью команды `git resert HEAD`

```
$ git status
On branch lab_02
Your branch is ahead of 'origin/lab_02' by 1 commit.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   lab_02_1_5/untils.c
        modified:   readme.txt

$ git reset HEAD . 
Unstaged changes after reset:
M       lab_02_1_5/untils.c
M       readme.txt
```
## 18. Файл readme.txt верните в исходное состояние.

Любой файл можно вернуть в исходное состояние (состояние последнего коммита) можно с помощью команды `git checkout`.

```
$ git checkout readme.
$ git diff readme.txt
```

## 19. Зафиксируйте изменения в измененных файлах по одному.

Зафиксировать изменения можно с помощью комманды `git commit`, предваритель добавив файлы по версионный контроль.

```
$ git status
On branch lab_02
Your branch is ahead of 'origin/lab_02' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   lab_02_1_5/untils.c

no changes added to commit (use "git add" and/or "git commit -a")

$ git add lab_02_1_5/untils.c 
volodya@DESKTOP-OU40H30:/mnt/c/Users/volodya/Desktop/iu7-cprog-labs-2020-larinvladimir$ git commit -m "(opi) add descriptions 
for each function"
[lab_02 ed0f98f] (opi) add descriptions for each function
 1 file changed, 19 insertions(+)
```

## 20. Что нужно сделать, чтобы файлы с расширениями «о» и «ехе» не «интересовали» git? Сделайте это.

Чтобы файлы с расширениями `o` и `exe` не добавлялись в индекс, необходимо добавить в репозиторий файл `.gitignore`, в котором будут написаны шаблоны имени неиндексируемых файлов.

```
$ git add .gitignore 

$ git diff --cached
diff --git a/.gitignore b/.gitignore
new file mode 100644
index 0000000..be6828c
--- /dev/null
+++ b/.gitignore
@@ -0,0 +1,3 @@
+*.o
+*.exe
+*.out
```

## 21. Удалите файл readme.txt из-под версионного контроля.

Чтобы удалить файл из-под версионного контроля необходимо воспользоваться командой `git rm`
```
$ git rm readme.txt
rm 'readme.txt'
```
